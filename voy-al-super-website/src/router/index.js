import Vue from 'vue'
import VueRouter from 'vue-router'
import MainScreen from '@/components/mainScreen.vue'
import Login from '@/components/login.vue'
import workspace from '@/components/workspace'
import firebase from '../firebase'
require('firebase/auth')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: MainScreen
  },
  {
    path: '/login',
    name: 'Log-In',
    component: Login
  },
  {
    path: '/workspace',
    name: 'Workspace',
    component: workspace,
    meta: {requiresAuth:true}
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// Para lo de navegacion segura
router.beforeEach((to, from, next) => {
  // lo que hace esta constante booleana es preguntar si cada
  // ruta requiere autenticacion
  console.log(from)
  console.log(next)
  const routeAuth = to.matched.some(record => record.meta.requiresAuth);
  // para ver si el usuario hizo el login
  const user = firebase.auth().currentUser;
  if(routeAuth && user == null){
    next({name:'Home'});
  } else {
    next();
  }
});

export default router
