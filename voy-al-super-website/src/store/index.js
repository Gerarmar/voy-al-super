import Vue from "vue"
import Vuex from "vuex"
import router from '../router/index'
import app from "../firebase"
require('firebase/auth')
require('firebase/database')

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        usr:'',
        error:''
    },
    mutations: {
        setUsr(state, value){
            state.usr = value;
        },
        setError(state, value){
            state.error = value;
        }
    },
    actions: {
        loginToAcc({commit}, user){
            app
            .auth()
            .signInWithEmailAndPassword(user.email, user.pass)
            .then(res => {
                console.log("entre a la cuenta ya registrada")
                commit("setUsr",{email:res.user.email, uid:res.user.uid})
                router.push({name:'Workspace'})
            }).catch((e) => {
                console.log(e.message)
                commit("setError", e.message)
            })
        },
        logout({commit}){
            app
            .auth()
            .signOut()
            .then(res => {
                console.log(res)
                router.push({name:'Home'})
            }).catch((e) => {
                console.log(e.message)
                commit("setError", e.message)
            })
        },
        registerAcc({commit}, user){
            app.auth()
            .createUserWithEmailAndPassword(user.email, user.pass)
            .then(res=>{
                app.database().ref('users').push({
                    uid:res.user.uid,
                    name:user.name,
                    email:user.email
                })
                router.push({name:'Workspace'})
            }).catch(e=>{
                commit('setError', e.message)
            })
        }
    },
    modules: {}
})